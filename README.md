# fluentd-loggly 

## Requirements
- `kustomize`
- `kubectl`

## Quick Start
This repo contains a `fluentd-kubernetes-daemonset` application to export logs to
a configured Loggly instance.

Configure it by creating two config files in this directory : 
- `loggly.config`
```
LOGGLY_TAGS=tag1,tag2
```
- `loggly-token.env`
```
LOGGLY_TOKEN=<YOUR_LOGGLY_CLIENT_TOKEN>
```

Build and apply the configured application against a target cluster
```
kustomize build | kubectl apply -f -
```

## Description

### Default Behavior

[fluentd-kubernetes-daemonset](https://github.com/fluent/fluentd-kubernetes-daemonset)
is a log-exporter application with an option to [target Loggly](https://github.com/fluent/fluentd-kubernetes-daemonset/blob/master/docker-image/v1.7/debian-loggly/conf/fluent.conf).

It captures log output from all running containers, annotates it with Kubernetes relevant context,
and forwards to Loggly. 

An enriched log line for a sample cert-manager output looks like:
```json
{
    "log": "original log line to stderr or stdout",
    "stream": "stderr",
    "docker": {
        "container_id": "5117fbe9a6ca4d6693e5326a51d2350f2ce25ecdd55e95e7ad4dbdc617cc061b"
    },
    "kubernetes": {
        "container_name": "cert-manager",
        "namespace_name": "cert-manager",
        "pod_name": "cert-manager-cainjector-6865d46f55-s97tp",
        "container_image": "quay.io/jetstack/cert-manager-cainjector:v0.11.0",
        "container_image_id": "docker-pullable://quay.io/jetstack/cert-manager-cainjector@sha256:cf77d14d1c825190a38ac6b593f591998e3b34464f626f24479eb3e21dd589b3",
        "pod_id": "5e9fefa9-8a40-11ea-bdc3-42010a8a00ac",
        "host": "gke-xand-dev-test-tr-xand-dev-test-tr-b245d272-drbm",
        "labels": {
            "app": "cainjector",
            "pod-template-hash": "6865d46f55"
        },
        "master_url": "https://10.27.240.1:443/api",
        "namespace_id": "5d1551dd-8a40-11ea-ad87-42010a8a0073"
    }
}
```
 
### Configuration and Filters

Additional fluentd configuration files can be incorporated. The base application follows a convention
to include all `*.conf` files it finds in the directory `fluentd/etc/conf.d` [as part of it's initialization](https://github.com/fluent/fluentd-kubernetes-daemonset/blob/465b0bed769d5e7a5956317663878c6831bfbce7/templates/conf/fluent.conf.erb#L11)

These additional files can filter, enrich, and transform the stream of logs before exporting to the target sink.

This repo selectively includes filters found in `./filters` to configure and deploy the application.

## Working in this repo

See the [`./tests/docker-compose`](tests/docker-compose/README.md) README to run tests locally 
to verify the added `.conf` files.

See the [`./tests/k8s-test-apps`](./tests/k8s-test-apps/README.md) README to test these configurations
in live clusters with some utility K8s apps.

