##
## Publish
##

.publish:
  image: $CI_IMG
  stage: publish
  tags:
    - kubernetes-runner

# Required variables:
# - PACKAGE_VERSION
# - ZIP_BASE_FILENAME
.package-script: &package-script
  # Verify required vars are present
  - if [ -z "$PACKAGE_VERSION" ]; then echo "Must set PACKAGE_VERSION variable" && exit 1; fi;
  - if [ -z "$ZIP_BASE_FILENAME" ]; then echo "Must set ZIP_BASE_FILENAME variable" && exit 1; fi;
  - echo $PACKAGE_VERSION
  # Build zip filename with version incorporated
  - export ZIP_FILENAME="$ZIP_BASE_FILENAME.$PACKAGE_VERSION.zip"
  - zip $ZIP_FILENAME version.txt README.md
  - pushd k8s
  - zip -r ../$ZIP_FILENAME ./
  - popd

# Required variables
# - ZIP_BASE_FILENAME
# - ZIP_FILENAME
# - ARTIFACTORY_ARTIFACTS_URL
.publish-script: &publish-script
  - if [ -z "$ARTIFACTORY_ARTIFACTS_URL" ]; then echo "Must set ARTIFACTORY_ARTIFACTS_URL variable" && exit 1; fi;
  - if [ -z "$ZIP_BASE_FILENAME" ]; then echo "Must set ZIP_BASE_FILENAME variable" && exit 1; fi;
  - if [ -z "$ZIP_FILENAME" ]; then echo "Must set ZIP_FILENAME variable" && exit 1; fi;
  # Build properties to upload with package
  - export PROPERTIES="job_url=$CI_JOB_URL;branch=$CI_COMMIT_REF_NAME;pipeline_id=$CI_PIPELINE_IID"
  # Upload to Artifactory
  - curl --fail -u$ARTIFACTORY_USER:$ARTIFACTORY_PASS -T $ZIP_FILENAME "$ARTIFACTORY_ARTIFACTS_URL/$ZIP_BASE_FILENAME/$ZIP_FILENAME;$PROPERTIES"

.publish-variables: &publish-variables
  ARTIFACTORY_ARTIFACTS_URL: "https://transparentinc.jfrog.io/artifactory/artifacts-internal"

.except-schedules-triggers:
  except:
    - schedules
    - pipelines

# This removes the CRI parser type environment variable, which will cause fluentd to use
# the default JSON parser.
# The CRI praser enables fluentd to parse containerd logs, which are in CRI format. Conversely,
# not setting a parser type enables the default JSON parser which enables fluentd to
# parse dockerd logs. This allows us to produce two loggly zips. The defualt zip for k8s
# clusters that run containerd (azure & google), and the other for dockerd clusters (AWS).
.remove-cri-parser-type: &remove-cri-parser-type
  # removes the env variable name 'FLUENT_CONTAINER_TAIL_PARSER_TYPE', and 1 line after which is the 'value'.
  - sed -i '/FLUENT_CONTAINER_TAIL_PARSER_TYPE/,+1d' k8s/loggly.yaml

# Default beta jobs per package
beta-publish-fluentd-loggly:
  extends:
    - .publish
  when: manual
  except:
    refs:
      - master
  variables:
    <<: *publish-variables
    ZIP_BASE_FILENAME: "fluentd-loggly-k8s"
  script:
    # Build package version with beta suffix
    - export PACKAGE_VERSION=$(cat version.txt)-beta.$CI_PIPELINE_IID
    - *package-script
    - *publish-script

# Default beta jobs per package
beta-publish-fluentd-loggly-dockerd:
  extends:
    - .publish
  when: manual
  except:
    refs:
      - master
  variables:
    <<: *publish-variables
    ZIP_BASE_FILENAME: "fluentd-loggly-k8s"
  script:
    # Build package version with beta suffix
    - export PACKAGE_VERSION=$(cat version.txt)-dockerd-beta.$CI_PIPELINE_IID
    - *remove-cri-parser-type
    - *package-script
    - *publish-script

# Default stable jobs per package
stable-publish-fluentd-loggly:
  extends:
    - .publish
    - .except-schedules-triggers
  variables:
    <<: *publish-variables
    ZIP_BASE_FILENAME: "fluentd-loggly-k8s"
  only:
    refs:
      - master
  script:
    # Get package version
    - export PACKAGE_VERSION=$(cat version.txt)
    - *package-script
    - *publish-script

# Default stable jobs per package
stable-publish-fluentd-loggly-dockerd:
  extends:
    - .publish
    - .except-schedules-triggers
  variables:
    <<: *publish-variables
    ZIP_BASE_FILENAME: "fluentd-loggly-k8s"
  only:
    refs:
      - master
  script:
    # Get package version
    - export PACKAGE_VERSION=$(cat version.txt)-dockerd
    - *remove-cri-parser-type
    - *package-script
    - *publish-script


