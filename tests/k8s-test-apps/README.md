## noise k8s app 

`noise.deployment.yaml` is a minimal K8s Deployment apply-able to any cluster.

It increments and prints a counter + a datetime stamp every 15s.
 
 By default, these definitions deploy 10 replicas into a `noise` namespace

Apply it with
```
kubectl create ns noise
kubectl apply -f noise.deployment.yaml
```

Clean it up with 
```
kubectl delete -f noise.deployment.yaml
kubectl delete ns noise
```

Use it when needing an app to run indefinitely and print to stdout.

## Example Usage
Deploy the app, and every couple minutes, toggle the filters applied for `fluent-loggly` to include or exclude logs
from the `noise` namespace.

You can then visit Loggly to see the ebb and flow of logs, verifying if the filters configured  
are working or not.

Example:
![Loggly Logs over 10 minute Filter Toggles](../images/noise-namespace-filter-verified.png)
