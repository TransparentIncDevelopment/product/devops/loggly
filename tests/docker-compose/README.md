Run the test with:
```
make test
```

This test starts a `fluent/fluentd-kubernetes-daemonset:v1.7-debian-loggly-1` container configured
with the filters found in `/filters`. It ingests the `./inputs/test.log` file, and emits output to
`./output/output.log`, and verifies against `./expected/output.log`.

Add filters outside this `tests` directory by symlinking to them.
```
symlink ../../../filters/filter-namespace-noise.conf filter-namespace-noise.conf 
``` 




